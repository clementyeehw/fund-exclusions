## Import Python libraries ## 
from bs4 import BeautifulSoup
import datetime as dt
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import numpy as np
import os
import pandas as pd
import PyPDF2
import re
import requests
from selenium import webdriver
import shutil
import smtplib
import ssl
import tabula
import time
import urllib
import zipfile


# Initialise global parameters #
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0'}
opener = urllib.request.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0')]
ssl._create_default_https_context = ssl._create_unverified_context
urllib.request.install_opener(opener) 


# Initialise a Chrome Session #
def start_webpage(log_path, download_path=None):
    ''' 
    Initialise a Chrome browser session.
    Parameters:
        log_path      : String containing the filepath of the geckodriver log file
        download_path : String containing the local download filepath.
                        Default value is None
    Return:
        browser       : Instantiated Chrome browser
    '''
    # Initialise a Chrome browser session #
    options = webdriver.ChromeOptions()
    options.add_argument("--start-maximized")
    options.add_argument('--headless')
    
    if download_path != None:
        options.add_experimental_option('prefs', 
                                        {'download.default_directory': download_path})
                       
    browser = webdriver.Chrome(options=options,
                               service_log_path=log_path)
    
    return browser


# Enrich URL library #
def enrich_urls(target_org, target_url):
    ''' 
    Send HTTP requests to website and return attachment download link.
    
    Parameters:
        target_org    : Organisation which url belongs to
        target_url    : Url that would be used to send http request
    Return:
        data          : Url that contains target data
    '''    
    # Send request to target website #
    response = requests.get(target_url, headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser')
    # Scoop all the urls #
    urls = soup.find_all('a', href=True)
    
    # Check target_org parameter #
    if 'TRANSITION PATHWAY INITIATIVE' in target_org:
        # Identify the url with sector list #
        data = [(target_org, 'https://www.transitionpathwayinitiative.org' + url['href']) 
                for url in urls 
                if re.search('download all sectors', url.get_text(), re.IGNORECASE)]
    
    elif 'APG ASSET MANAGEMENT - FUND EXCLUSION' in target_org:
        # Identify the url with exclusion list #
        data = [(target_org, 'https://apg.nl' + url['href']) 
                for url in urls 
                if url.find('div', 
                            text=re.compile('excluded companies', 
                                            re.IGNORECASE))]
        if len(data) > 0:
            data = [data[0]]
    
    elif 'APG ASSET MANAGEMENT - FUND ENGAGEMENT' in target_org:
        # Identify the url with exclusion list #
        data = [(target_org, 'https://apg.nl' + url['href']) 
                for url in urls 
                if url.find('div', 
                            text=re.compile('engagement with companies', 
                                            re.IGNORECASE))]
    
    elif ('SWEDISH NATIONAL PENSION FUNDS - FIRST' in target_org):
        # Identify the url with exclusion list #
        data = [(target_org, url['href']) 
                for url in urls 
                if 'EXCLUSION' in url.text.upper()]
    
    elif ('IRELAND STRATEGIC INVESTMENT FUND - FOSSIL FUEL' in target_org):
        # Identify the url with exclusion list #
        data = [(target_org, url['href']) 
                for url in urls 
                if ('FF-LIST' in url['href'].upper()) and \
                ('.PDF' in url['href'].upper())]
    
    elif 'NZ SUPER FUND' in target_org:
        data = [(target_org, 'https://nzsuperfund.nz' + url['href']) 
                for url in urls 
                if ('EXCLUSION' in url['href'].upper()) and \
                ('.XLSX' in url['href'].upper())]
        
    else:
        # Return empty list for data #
        data = []
    
    return data


# Download the records and update summary text file #
def download_file(source, attachments=True):
    ''' 
    Initialise download sequence for source or processed files.
    
    Parameters:
        source        : Source table that contains organisations and 
                        their associated weblink
        attachments   : Flag to indicate whether the download process 
                        is related to an attachment. Default value is True
    '''  
    # Sieve out organisations with downloadable attachments #
    pdf_orgs = source.loc[source['ATTACH_FLAG'].notnull()]
    url_attachments = [(pdf_orgs['ORGANISATION'].iloc[i], 
                        pdf_orgs['LOCAL_DOWNLOAD_PATH'].iloc[i],
                        pdf_orgs['SCRAPE_LINK'].iloc[i]) 
                        for i in range(len(pdf_orgs))]
    other_orgs = source.loc[source['ATTACH_FLAG'].isnull()]
    url_no_attachments = [(other_orgs['ORGANISATION'].iloc[i], 
                           other_orgs['LOCAL_DOWNLOAD_PATH'].iloc[i],
                           other_orgs['SCRAPE_LINK'].iloc[i]) 
                           for i in range(len(other_orgs))]
        
    # Initialise other parameters #
    date_today = dt.datetime.now().strftime('%Y%m%d')
    
    # Commence with each entity in url_attachments #
    if attachments:
        for org, download_path, source_loc in url_attachments:
            # Check organisation to determine actions taken #
            if org == 'SCIENCE BASED TARGETS':
                print('Download initiated for %s' % org)
                filename = 'SBTI_COMPANIES_%s.csv' % date_today
                try:
                    sbt_download(filename, download_path, source_loc)
                    get_SBTI_companies(filename, download_path, date_today)
                    os.remove(download_path + '\\' + filename)
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org)     
            elif org == 'TRANSITION PATHWAY INITIATIVE':
                print('Download initiated for %s' % org)
                try:
                    tpi_download(date_today, download_path, source_loc)
                    tpi_download(date_today, re.sub('Original', 'Editable', download_path), 
                                 source_loc)
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org)     
            elif org == 'SWEDISH NATIONAL PENSION FUNDS - FIRST':
                filename = 'AP1_EXCLUSIONS_%s.pdf' % date_today
                send_request(org, source_loc, filename, download_path, date_today)
                
            elif org == 'APG ASSET MANAGEMENT - FUND EXCLUSION':
                filename = 'APG_EXCLUSIONS_%s.pdf' % date_today
                send_request(org, source_loc, filename, download_path, date_today)
                
            elif org == 'APG ASSET MANAGEMENT - FUND ENGAGEMENT':
                filename = 'APG_ENGAGEMENTS_%s.pdf' % date_today
                send_request(org, source_loc, filename, download_path, date_today)
                
            elif org == 'IRELAND STRATEGIC INVESTMENT FUND - FOSSIL FUEL':
                filename = 'ISIF_FF_EXCLUSIONS_%s.pdf' % date_today
                send_request(org, source_loc, filename, download_path, date_today)              
                
            elif org == 'NZ SUPER FUND':
                # Download list of company exclusions #
                filename = 'NZSF_RAW_DATA_%s.xlsx' % date_today
                add_path = source.loc[
                                      (source['ORGANISATION'] == 
                                      'NZ FOREIGN AFFAIRS AND TRADE - UN SANCTIONS', 'LOCAL_DOWNLOAD_PATH')].iloc[0] + \
                                      '\\' + 'NZMFAT_EXCLUSIONS_%s.xlsx' % date_today
                send_request(org, source_loc, filename, download_path, date_today, add_path) 
                # Download press releases #
                press_releases = [file for file in os.listdir(download_path) if re.search(r'.pdf', file)]
                
                # Send GET request to NZ Super Fund's website #
                website = source.loc[source['ORGANISATION'] == org, 'ORG_LINK'].iloc[0]
                response = requests.get(website, headers=headers)
                soup = BeautifulSoup(response.text, 'html.parser')
                
                # Get all the new PDF press releases # 
                web_releases = soup.find_all('a', attrs={'class': 'sideDocLink'}, href=True)
                web_releases = [(web_release.text, web_release['href']) 
                                for web_release in web_releases 
                                if '.pdf' in web_release['href'].lower()]
                download_releases = [urllib.request.urlretrieve(weblink, download_path + '\\' + filename + '.pdf') 
                                    for filename, weblink in web_releases 
                                    if not filename in press_releases]
                # Archive a copy in Editable folder #
                download_releases = [urllib.request.urlretrieve(weblink, 
                                                                re.sub('Original', 'Editable', download_path) + \
                                                                '\\' + filename + '.pdf') 
                                    for filename, weblink in web_releases 
                                    if not filename in press_releases]
                
    # Commence with each entity without attachments, i.e. obtained via web-scrapping in url_no_attachments #
    else:
        for org, download_path, source_loc in url_no_attachments:
            # Check organisation to determine actions taken #
            if org == 'NORGES BANK INVESTMENT MANAGEMENT':                
                # Run function #
                print('Download initiated for %s' % org)
                try:
                    get_NBIM_Exclusions(source_loc, download_path, date_today)
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org)
              
            elif org == 'IRELAND STRATEGIC INVESTMENT FUND - CLUSTER MUNITIONS':                
                # Run function #
                print('Download initiated for %s' % org)
                try:
                    get_ISIF_CM_Exclusions(source_loc, download_path, date_today)
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org)
            
            elif org == 'SVVK ASIR':                
                # Run function #
                print('Download initiated for %s' % org)
                try:
                    get_SVVK_Exclusions(source_loc, download_path, date_today)
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org)

            elif org == 'SWEDISH NATIONAL PENSION FUNDS - SECOND':
                # Run function #
                print('Download initiated for %s' % org)
                try:
                    get_AP2_Exclusions(source_loc, download_path, date_today)   
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org)
            
            elif org == 'SWEDISH NATIONAL PENSION FUNDS - THIRD':
                # Run function #
                print('Download initiated for %s' % org)
                try:
                    get_AP3_Exclusions(source_loc, download_path, date_today)   
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org)
            
            elif org == 'SWEDISH NATIONAL PENSION FUNDS - FOURTH':
                # Run function #
                print('Download initiated for %s' % org)
                try:
                    get_AP4_Exclusions(source_loc, download_path, date_today)   
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org)
                    
            elif org == 'SWEDISH NATIONAL PENSION FUNDS - COUNCIL ON ETHICS':
                # Run function #
                print('Download initiated for %s' % org)
                try:
                    get_APSCE_Exclusions(source_loc, download_path, date_today)   
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org)         
                    
            elif org == 'NZ FOREIGN AFFAIRS AND TRADE - UN SANCTIONS':
                # Run function #
                print('Download initiated for %s' % org)
                try:
                    get_NZMFAT_Exclusions(source_loc, download_path, date_today)   
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org) 
            
            elif org == 'FINANCE FOR BIODIVERSITY':
                # Run function #
                print('Download initiated for %s' % org)
                try:
                    get_FBP_companies(source_loc, download_path, date_today)   
                except Exception as e:
                    print(e)
                else:
                    print('Download completed for %s' % org) 
                    

# Create a Chrome session to download the SBT file #
def sbt_download(filename, download_path, source_loc):
    '''
    Create a Chrome session to download the SBT file.
    
    Parameters:
    filename      : Filename used to save the relevant raw data files
    download_path : Local file path that is used to deposit the relevant raw data files
    source_loc    : URL to send GET request
    '''
    # Initialise a Chrome browser #      
    browser = start_webpage(log_path = download_path + '\\geckodriver.log',
                            download_path = download_path)
    
    # Send GET request to weblink #            
    browser.get(source_loc)
    
    # Wait for browser to complete loading #
    time.sleep(5)
    
    # Accept cookies #
    try:
        button = browser.find_element_by_xpath(
                        "//a[@href='https://sciencebasedtargets.org/companies-taking-action?cookies=yes']")
    except Exception:
        pass
    else:
        button.click()
                
    # Select Download button #
    time.sleep(3)
    button = browser.find_element_by_xpath("//button[@class='o-btn o-btn--sm inline-block']")
    try:
        button.click()
    except Exception:
        # Close Read More iframe #
        browser.switch_to.frame(browser.find_element_by_tag_name('iframe'))
        button = browser.find_element_by_class_name('icon-close')
        browser.execute_script('arguments[0].click();', button)
        browser.switch_to.default_content()
    else:
        button = browser.find_element_by_xpath("//button[@class='o-btn o-btn--sm inline-block']")
        button.click()
    
    # Close browser when download is complete #
    while True:
        flag = [file for file in os.listdir(download_path) if file == 'companies-taking-action.csv']
        if flag:
            ## Rename file ##
            os.rename(download_path + '\\' + flag[0], download_path + '\\' + filename)
            ## Close browser ##
            browser.close()
            ## Break loop ##
            break
        else:
            time.sleep(2)


# Create a Chrome session to download the TPI zipped files #
def tpi_download(date_today, download_path, source_loc):
    '''
    Create a Chrome session to download the TPI zipped files.
    
    Parameters:
    date_today    : Today's date
    download_path : Local file path that is used to deposit the relevant raw data files
    source_loc    : URL to send GET request
    '''
    # Download zip file #
    response = requests.get(source_loc, headers=headers)
    with open(download_path + r'\TPI sector.zip', 'wb') as zfile:
        zfile.write(response.content)
    
    # Migrate files to respective folders when download is complete #
    while True:
        flag = [file for file in os.listdir(download_path) if re.search('TPI sector', file)]
        if flag:
            ## Unzip all files ##
            with zipfile.ZipFile(download_path + '\\' + flag[0], 'r') as zip_ref:
                zip_ref.extractall(download_path)
            ## Create waiting time ##
            time.sleep(5)
            ## Remove original zipped file ##
            os.remove(download_path + '\\' + flag[0])
            ## Rename and migrate each unzipped file ##
            for file in os.listdir(download_path):
                filename = file.split('.csv')[0].upper()
                if re.search(r'Company_Latest_Assessments', file, re.IGNORECASE):
                    os.rename(download_path + '\\' + file, 
                              download_path + r'\Company Assessments' + '\\' + \
                                  filename + '_' + date_today + '.csv')    
                elif re.search(r'CP_Assessments', file, re.IGNORECASE):
                    filename = '_'.join(filename.split('_')[:-1])
                    os.rename(download_path + '\\' + file, 
                              download_path + r'\CP Assessments' + '\\' + \
                                  filename + '_' + date_today + '.csv')
                elif re.search(r'MQ_Assessments_Methodology_1', file, re.IGNORECASE):
                    filename = '_'.join(filename.split('_')[:-1])
                    os.rename(download_path + '\\' + file, 
                              download_path + r'\MQ Assessments 1' + '\\' + \
                                  filename + '_' + date_today + '.csv')
                elif re.search(r'MQ_Assessments_Methodology_2', file, re.IGNORECASE):
                    filename = '_'.join(filename.split('_')[:-1])
                    os.rename(download_path + '\\' + file, 
                              download_path + r'\MQ Assessments 2' + '\\' + \
                                  filename + '_' + date_today + '.csv')
                elif re.search(r'MQ_Assessments_Methodology_3', file, re.IGNORECASE):
                    filename = '_'.join(filename.split('_')[:-1])
                    os.rename(download_path + '\\' + file, 
                              download_path + r'\MQ Assessments 3' + '\\' + \
                                  filename + '_' + date_today + '.csv')
                elif re.search(r'Sector_Benchmarks', file, re.IGNORECASE):
                    filename = '_'.join(filename.split('_')[:-1])
                    os.rename(download_path + '\\' + file, 
                              download_path + r'\Sector Benchmarks' + '\\' + \
                                  filename + '_' + date_today + '.csv')
            ## Break loop ##
            break
        else:
            time.sleep(2)         
               
            
# Send request to website and run subsequent processes #
def send_request(org, source_loc, filename, download_path, date_today, add_path = None):
    ''' 
    Send request to website and run subsequent processes.     

    Parameters:
        org           : Name of organisation that we are scrapping the information
        source_loc    : URL to send GET request
        filename      : Filename used to save the relevant raw data files
        download_path : Local file path that is used to deposit the relevant raw data files
        date_today    : Today's date
        add_path      : Additional files to be considered for subsequent processes.
                        Default value is None
    Return:
        latest_file   : Name of the immediate preceding file
    '''   
    # Output initiation status #
    print('Download initiated for %s' % org)
    
    # Send GET request #
    while True:
        try:
            urllib.request.urlretrieve(source_loc, download_path + '\\' + filename)
        except Exception as e:
            # Print error message #
            print(e)
        else:
            # Set waiting time of 3 seconds for downloading process #
            time.sleep(3)
            # Run function for subsequent processes if successful #
            try:
                if org == 'SWEDISH NATIONAL PENSION FUNDS - FIRST':
                    get_AP1_Exclusions(filename, download_path, date_today)
            
                elif org == 'APG ASSET MANAGEMENT - FUND EXCLUSION':
                    get_APG_Exclusions(filename, download_path, date_today)
                
                elif org == 'APG ASSET MANAGEMENT - FUND ENGAGEMENT':
                    get_APG_Engagements(filename, download_path, date_today)    
            
                elif org == 'IRELAND STRATEGIC INVESTMENT FUND - FOSSIL FUEL':        
                    get_ISIF_FF_Exclusions(filename, download_path, date_today)
                
                elif org == 'NZ SUPER FUND':        
                    get_NZSF_Exclusions(filename, download_path, date_today, add_path)
                
            except Exception as e:
                # Print error message #
                print(e)
                # Break while loop #
                break
            else:
                # Output completion status #
                print('Download completed for %s' % org)
                # Break while loop #
                break
            

# Identify the immediate preceding file #
def get_latest_preceding_file(download_path, date_today, regex_string):
    ''' 
    Identify and return the latest dataset available in local directory.     

    Parameters:
        download_path : Local file path that is used to deposit the files
        regex_string  : Regular expression string to search for the particular file of interest
        date_today    : Today's date
    Return:
        latest_file   : Name of the immediate preceding file
    '''   
    filenames = [(file, dt.datetime.strptime(file.split('_')[-1].split('.xlsx')[0], '%Y%m%d')) 
                  for file in os.listdir(download_path) \
                  if re.search(r'%s' % regex_string, file)]
    latest_date = max([date 
                       for filename, date in filenames 
                       if date != dt.datetime.strptime(date_today, '%Y%m%d')])
    latest_file = [filename 
                   for filename, date in filenames 
                   if date == latest_date][0]
    
    return latest_file


# Output the updates/differences in text #
def download_updates(filename, download_path, add_list, drop_list, current_name, past_name, encoding = None):
    ''' 
    Output the differences from daily comparisons in a text file.     

    Parameters:
        filename      : Name of text file used to store the updates from the daily comparison
        download_path : Local file path that is used to deposit the files
        add_list      : DataFrame containing the list of organisations being added from last update
        drop_list     : DataFrame containing the list of organisations being removed from last update
        current_name  : Name of organisation column used in today's file
        past_name     : Name of organisation column used in last update
        encoding      : Encoding to be used to read text.
                        Default value is None
    '''   
    # Initialise date parameter in specified DD.MM.YYYY format #
    date_today = str(dt.datetime.now().strftime('%d.%m.%Y'))
    
    # Output the differences in text file #
    with open(download_path + '\\' + filename, 'a', encoding = encoding) as f:
        if len(add_list) != 0:
            for company in list(add_list[current_name]):
                f.write(company + ' is added on ' + date_today + '\n')
        else:
            f.write('No company is added on ' + date_today + '\n')
        if len(drop_list) != 0:
            for company in list(drop_list[past_name]):
                f.write(company + ' is removed on ' + date_today + '\n')
        else:
            f.write('No company is removed on ' + date_today + '\n')
    f.close()

    
# Web-scrapping scripts for each target website - Arranged in alphabetical order #
def get_AP1_Exclusions(filename, download_path, date_today):
    ''' 
    (1) Import the PDF list of AP1 exclusion companies from local directory
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data

    Parameters:
        filename      : Filename used to save the relevant raw data files
        download_path : Local file path that is used to deposit the relevant raw data files
        date_today    : Today's date
    '''   
    # Initialise parameters #
    df = pd.DataFrame()
    
    # Get last page number of PDF file #
    last_page = PyPDF2.PdfFileReader(open(download_path + '\\' + filename, 'rb')).getNumPages()
    
    # Parse the PDF file in the local directory #
    for page_num in range(1, last_page + 1):
        try:
            df_partial = tabula.read_pdf(download_path + '\\' + filename, 
                                         pages=page_num,
                                         area=(131,74.2,835.4,522.9),
                                         stream=True)[0]
        except Exception:
            df_partial = tabula.read_pdf(download_path + '\\' + filename, 
                                         pages=page_num,
                                         area=(131,74.2,835.4,522.9),
                                         stream=True)
        finally:
            # Concatenate the partial results with the full DataFrame #
            df = pd.concat([df, df_partial])        
            # Output status #
            print('Conversion Success: Page %s of %s' % (str(page_num), str(last_page)))
            
    if len(df) > 0:
        
        # Scan the directory and import the previous working day's dataset #
        latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'AP1_EXCLUSIONS_\d+.xlsx')
        df_past = pd.read_excel(download_path + '\\' + latest_file)
        
        # Manipulate present and past columns #
        df.columns = ['COMPANY', 'STOCK_ISIN']
        df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
        past_company = ''.join([word for word in list(df_past.columns) if 'COMPANY' in word])
        past_isin = ''.join([word for word in list(df_past.columns) if 'ISIN' in word])
        
        # Identify the list of additions and deletions #
        pd.options.mode.chained_assignment = None
        comparison = pd.merge(df, df_past, how = 'outer', left_on = 'STOCK_ISIN', right_on = past_isin)
        add_list = comparison.loc[comparison[past_isin].isnull()]
        if len(add_list) != 0:
            add_list['COMPANY'] = add_list['COMPANY'] + '-' + add_list['STOCK_ISIN']
        drop_list = comparison.loc[comparison['STOCK_ISIN'].isnull()]
        if len(drop_list) != 0:
            drop_list[past_company] = drop_list[past_company] + '-' + drop_list[past_isin]
        
        # Output the differences in text file #  
        download_updates('AP1_UPDATES.txt', download_path, add_list, drop_list, 'COMPANY', past_company, encoding = None)
        download_updates('AP1_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                         add_list, drop_list, 'COMPANY', past_company, encoding = None)
        pd.options.mode.chained_assignment = 'warn'
        
        # Download data #
        df.to_excel(download_path  + '\\' + re.sub('.pdf','.xlsx', filename), 
                    sheet_name = 'AP1_EXCLUSIONS', 
                    index = False)


def get_AP2_Exclusions(target_url, download_path, date_today):
    ''' 
    (1) Connect to AP2 website and scrape the list of exclusion companies
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data

    Parameters:
        target_url    : Url for exclusion companies in AP2
        download_path : Local file path to deposit exclusion companies in AP2
        date_today    : Today's date
    '''   
    # Create filename for respective entity #
    filename = 'AP2_EXCLUSIONS_%s.xlsx' % date_today
                
    # Import data and manipulate columns #
    df = pd.read_html(target_url, header=0)[0]
    current_name = ''.join([word for word in list(df.columns) if 'COMPANY' in word.upper()])
   
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'AP2_EXCLUSIONS_\d+')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
     
    # Manipulate past columns #
    df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
    past_name = ''.join([word for word in list(df_past.columns) if 'COMPANY' in word.upper()])

    # Identify the list of additions and deletions #
    comparison = pd.merge(df, df_past, how = 'outer', left_on = current_name, right_on = past_name)
    add_list = comparison[comparison[past_name].isnull()][[current_name]]
    drop_list = comparison[comparison[current_name].isnull()][[past_name]]    

    # Output the differences in text file #  
    download_updates('AP2_UPDATES.txt', download_path, add_list, drop_list, current_name, past_name, encoding = None)
    download_updates('AP2_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                     add_list, drop_list, current_name, past_name, encoding = None)    
    
    # Download data #
    df.to_excel(download_path  + '\\' + filename, 
                sheet_name = 'AP2_EXCLUSIONS', 
                index = False)    


def get_AP3_Exclusions(target_url, download_path, date_today):
    ''' 
    (1) Connect to AP3 website and scrape the list of exclusion companies
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data

    Parameters:
        target_url    : Url for exclusion companies in AP3
        download_path : Local file path to deposit exclusion companies in AP3
        date_today    : Today's date
    '''   
    # Create filename for respective entity #
    filename = 'AP3_EXCLUSIONS_%s.xlsx' % date_today
                
    # Import data and manipulate columns #
    df = pd.read_html(target_url)[0]
    df.columns = ['COMPANY', 'STOCK_ISIN']
    
    # Pre-process concatenated rows due to merged STOCK_ISIN cells #
    df['STOCK_ISIN'] = df['STOCK_ISIN'].str.split()
    rows = [[df['COMPANY'][i], isin] for i in range(len(df['COMPANY'])) for isin in df['STOCK_ISIN'][i]]
    df = pd.DataFrame(rows, columns=df.columns)
       
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'AP3_EXCLUSIONS_\d+')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
    
    # Manipulate past columns #
    df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
    past_company = ''.join([word for word in list(df_past.columns) if 'COMPANY' in word])
    past_isin = ''.join([word for word in list(df_past.columns) if 'ISIN' in word])
        
    # Identify the list of additions and deletions #
    pd.options.mode.chained_assignment = None
    comparison = pd.merge(df, df_past, how = 'outer', left_on = 'STOCK_ISIN', right_on = past_isin)
    add_list = comparison.loc[comparison[past_isin].isnull()]
    if len(add_list) != 0:
        add_list['COMPANY'] = add_list['COMPANY'] + '-' + add_list['STOCK_ISIN']
    drop_list = comparison.loc[comparison['STOCK_ISIN'].isnull()]
    if len(drop_list) != 0:
        drop_list[past_company] = drop_list[past_company] + '-' + drop_list[past_isin]
    
    # Output the differences in text file #  
    download_updates('AP3_UPDATES.txt', download_path, add_list, drop_list, 'COMPANY', past_company, encoding = None)
    download_updates('AP3_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                     add_list, drop_list, 'COMPANY', past_company, encoding = None)
    pd.options.mode.chained_assignment = 'warn'
    
    # Download data #
    df.to_excel(download_path  + '\\' + filename, 
                sheet_name = 'AP3_EXCLUSIONS', 
                index = False) 


def get_AP4_Exclusions(target_url, download_path, date_today):
    ''' 
    (1) Connect to AP4 website and scrape the list of exclusion companies
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data

    Parameters:
        target_url    : Url for exclusion companies in AP4
        download_path : Local file path to deposit exclusion companies in AP4
        date_today    : Today's date
    '''   
    # Create filename for respective entity #
    filename = 'AP4_EXCLUSIONS_%s.xlsx' % date_today
                
    # Import data and manipulate columns #
    df = pd.read_html(target_url)[-1]
    current_name = ''.join([word for word in list(df.columns) if 'COMPANY' in word.upper()])
   
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'AP4_EXCLUSIONS_\d+')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
     
    # Manipulate past columns #
    df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
    past_name = ''.join([word for word in list(df_past.columns) if 'COMPANY' in word.upper()])

    # Identify the list of additions and deletions #
    comparison = pd.merge(df, df_past, how = 'outer', left_on = current_name, right_on = past_name)
    add_list = comparison[comparison[past_name].isnull()][[current_name]]
    drop_list = comparison[comparison[current_name].isnull()][[past_name]]    

    # Output the differences in text file #  
    download_updates('AP4_UPDATES.txt', download_path, add_list, drop_list, current_name, past_name, encoding = None)
    download_updates('AP4_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                     add_list, drop_list, current_name, past_name, encoding = None)
   
    # Download data #
    df.to_excel(download_path  + '\\' + filename, 
                sheet_name = 'AP4_EXCLUSIONS', 
                index = False) 
    

def get_APG_Engagements(filename, download_path, date_today):
    ''' 
    (1) Import the PDF list of APG engagement companies from local directory
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data

    Parameters:
        filename      : Filename used to save the relevant raw data files
        download_path : Local file path that is used to deposit the relevant raw data files
        date_today    : Today's date
    '''   
    # Initialise parameters #
    df = pd.DataFrame()
    
    # Get last page number of PDF file #
    last_page = PyPDF2.PdfFileReader(open(download_path + '\\' + filename, 'rb')).getNumPages()
    
    # Parse the PDF file in the local directory #
    for page_num in range(1, last_page + 1):
        try:
            df_partial = tabula.read_pdf(download_path + '\\' + filename, 
                                         pages=page_num,
                                         area=(123.148,47.25,767.534,562.164),
                                         stream=True)[0]
        except Exception:
            df_partial = tabula.read_pdf(download_path + '\\' + filename, 
                                         pages=page_num,
                                         area=(123.148,47.25,767.534,562.164),
                                         stream=True)
        finally:
            # Drop all NaN records #
            df_partial.dropna(subset=['Company'], inplace=True)
            # Concatenate the partial results with the full DataFrame for all valid data #
            df = pd.concat([df, df_partial])
            # Output status #
            print('Conversion Success: Page %s of %s' % (str(page_num), str(last_page)))
            
    if len(df) > 0:
        
        # Manipulate present index and columns # 
        df.index = np.arange(0, len(df))
        df.columns = ['COMPANY', 'SAFE_WORKING', 'HUMAN_RIGHTS', 'CHILD_LABOUR', 'ANTI_CORRUPTION',	
                      'CORPORATE_GOVERNANCE', 'ENVIRONMENT']
        current_name = ''.join([word for word in list(df.columns) if 'COMPANY' in word.upper()])
        
        # Find out consecutive numbers in DataFrame that have null rows #
        row_nums = list(df[df[['SAFE_WORKING', 'HUMAN_RIGHTS', 'CHILD_LABOUR', 'ANTI_CORRUPTION',	
                               'CORPORATE_GOVERNANCE', 'ENVIRONMENT']].isnull().all(1)].index)
        consecutive_series = [str(n-1) + '-' + str(n) for n in row_nums]
        # Pre-process the current DataFrame to merge company names that are spread across consecutive rows # 
        for series in consecutive_series:
            first = int(series.split('-')[0])
            second = int(series.split('-')[-1])
            df.loc[first, 'COMPANY'] = df.loc[first, 'COMPANY'] + ' ' + df.loc[second, 'COMPANY']
        # Remove rows that do not have themes #
        df.drop(row_nums, inplace=True)
        
        # Scan the directory and import the previous working day's dataset #
        latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'APG_ENGAGEMENTS_\d+.xlsx')
        df_past = pd.read_excel(download_path + '\\' + latest_file)
        
        # Manipulate past columns # 
        df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
        past_name = ''.join([word for word in list(df_past.columns) if 'COMPANY' in word.upper()])
        
        # Identify the list of additions and deletions #
        comparison = pd.merge(df, df_past, how = 'outer', left_on = current_name, right_on = past_name)
        add_list = comparison[comparison[past_name].isnull()][[current_name]]
        drop_list = comparison[comparison[current_name].isnull()][[past_name]]
    
        # Output the differences in text file #  
        download_updates('APG_ENGAGEMENT_UPDATES.txt', download_path, add_list, drop_list, 
                         current_name, past_name, encoding = 'utf-8')
        download_updates('APG_ENGAGEMENT_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                         add_list, drop_list, current_name, past_name, encoding = 'utf-8')
        
        # Download data #
        df.to_excel(download_path  + '\\' + re.sub('.pdf','.xlsx', filename), 
                    sheet_name = 'APG_ENGAGEMENTS', 
                    index = False)
    
    
def get_APG_Exclusions(filename, download_path, date_today):
    ''' 
    (1) Import the PDF list of APG exclusion companies from local directory
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data

    Parameters:
        filename      : Filename used to save the relevant raw data files
        download_path : Local file path that is used to deposit the relevant raw data files
        date_today    : Today's date
    '''   
    # Initialise parameters #
    df = pd.DataFrame()
    
    # Get last page number of PDF file #
    last_page = PyPDF2.PdfFileReader(open(download_path + '\\' + filename, 'rb')).getNumPages()
    
    # Parse the PDF file in the local directory #
    for page_num in range(1, last_page + 1):
        try:
            if page_num == 1:
                df_partial = tabula.read_pdf(download_path + '\\' + filename, 
                                         pages=page_num,
                                         area=(188.628,46.506,769.022,536.865))[0]
            else:
                df_partial = tabula.read_pdf(download_path + '\\' + filename, 
                                         pages=page_num,
                                         area=(134.309,47.994,757.117,553.979))[0]
        except Exception:
            if page_num == 1:
                df_partial = tabula.read_pdf(download_path + '\\' + filename, 
                                         pages=page_num,
                                         area=(188.628,46.506,769.022,536.865))
            else:
                df_partial = tabula.read_pdf(download_path + '\\' + filename, 
                                         pages=page_num,
                                         area=(134.309,47.994,757.117,553.979))
        finally:
            # Concatenate the partial results with the full DataFrame except last page #
            if page_num < last_page:
                df = pd.concat([df, df_partial])
            else:
                # Special handling for last page #
                # Append the first table to df by dropping the NaN entries in last column #
                exclusion = [column for column in list(df_partial.columns) if 'EXCLUSION' in column.upper()][0]
                df_temp = df_partial.dropna(subset=[df_partial.columns[-1]])
                df_temp = df_temp[[column for column in df_temp.columns 
                                   if not re.search('unnamed', column, re.IGNORECASE)]]
                if len(df_temp) > 0:
                    df_temp['Category'] = df_temp[exclusion].str.split('Product').str[-1].str.strip()
                    df_temp.loc[df_temp['Category'].str.contains('UN'), 'Category'] = np.nan 
                    df_temp['Ground for exclusion'] = np.nan
                    df_temp.loc[~df_temp[exclusion].str.contains('UN'), 'Ground for exclusion'] = 'Product'
                    df_temp.loc[df_temp[exclusion].str.contains('UN'), 'Ground for exclusion'] = \
                        df_temp.loc[df_temp[exclusion].str.contains('UN'), exclusion]
                    df_temp = df_temp[['Ground for exclusion', 'Category', 'Company', 'Country']]
                    df = df.append(df_temp, sort=False, ignore_index=True)
                # Fetch and manipulate the second table #
                row_num = df_partial.loc[df_partial[exclusion].str.contains('Government bonds')].index[0]
                table = df_partial[row_num+2:].dropna(axis=1)
                table.columns = ['Ground for exclusion', 'Country']
                # Append the second table to df #
                df = df.append(table, sort=False, ignore_index=True)
            # Output status #
            print('Conversion Success: Page %s of %s' % (str(page_num), str(last_page)))
            
    if len(df) > 0:
        
        # Scan the directory and import the previous working day's dataset #
        latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'APG_EXCLUSIONS_\d+.xlsx')
        df_past = pd.read_excel(download_path + '\\' + latest_file)
        
        # Manipulate present and past columns #
        current_company = ''.join([word for word in list(df.columns) if 'COMPANY' in word.upper()])
        current_country = ''.join([word for word in list(df.columns) if 'COUNTRY' in word.upper()])
        df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
        past_company = ''.join([word for word in list(df_past.columns) if 'COMPANY' in word.upper()])
        past_country = ''.join([word for word in list(df_past.columns) if 'COUNTRY' in word.upper()])
        
        # Manipulate present and past DataFrames #
        df_companies = df.loc[df[current_company].notnull()]
        df_countries = df.loc[df[current_company].isnull()]
        df_companies_past = df_past.loc[df_past[past_company].notnull()]
        df_countries_past = df_past.loc[df_past[past_company].isnull()]
        
        # Identify the list of company additions and deletions #
        comparison = pd.merge(df_companies, df_companies_past, 
                              how = 'outer', 
                              left_on = current_company, right_on = past_company)
        add_list = comparison[comparison[past_company].isnull()][[current_company]]
        drop_list = comparison[comparison[current_company].isnull()][[past_company]]
        
        # Output the differences in text file #  
        download_updates('APG_UPDATES.txt', download_path, add_list, drop_list, 
                         current_company, past_company, encoding = None)
        
        # Identify the list of country additions and deletions #
        comparison = pd.merge(df_countries, df_countries_past, 
                              how = 'outer', 
                              left_on = current_country, right_on = past_country)
        add_list = comparison[comparison[past_country].isnull()][[current_country]]
        drop_list = comparison[comparison[current_country].isnull()][[past_country]]
        
        # Initialise date parameter in specified DD.MM.YYYY format #
        date_today = str(dt.datetime.now().strftime('%d.%m.%Y'))
    
        # Output the differences in text file #
        paths = [download_path, re.sub('Original', 'Editable', download_path)]
        for path in paths:
            with open(path + '\\' + 'APG_UPDATES.txt', 'a') as f:
                if len(add_list) != 0:
                    for country in list(add_list[current_country]):
                        f.write(country + ' is added on ' + date_today + '\n')
                else:
                    f.write('No country is added on ' + date_today + '\n')
                if len(drop_list) != 0:
                    for country in list(drop_list[past_country]):
                        f.write(country + ' is removed on ' + date_today + '\n')
                else:
                    f.write('No country is removed on ' + date_today + '\n')
            f.close()
        
        # Download data #
        df.to_excel(download_path  + '\\' + re.sub('.pdf','.xlsx', filename), 
                    sheet_name = 'APG_EXCLUSIONS', 
                    index = False)
        
       
def get_APSCE_Exclusions(target_url, download_path, date_today):
    ''' 
    (1) Connect to Swedish Council on Ethics website and scrape the list of exclusion companies
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data 

    Parameters:
        target_url    : Url for Swedish Council's exclusion companies
        path          : Local file path to deposit Swedish Council's exclusion companies
        date_today    : Batch run date
    ''' 
    # Create filename for respective entity #
    filename = 'APSCE_EXCLUSIONS_%s.xlsx' % date_today
    
    # Initialise an empty array with shape (0,4): apsce_arr #
    apsce_arr = np.empty((0,4), str)
    
    # Send GET request to Swedish Council's website #
    response = requests.get(target_url, headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser')
    
    # Get all the related entities from the table rows # 
    rows = soup.find('tbody').find_all('tr')
    # # Column name #
    # columns = [column.text.strip() for column in rows[0].find_all('h4')]
    # Table data #
    for row in rows:
        entities = row.find_all('td')
        data = [entity.text.strip() for entity in entities]
        # Vectorize table data into array #
        apsce_arr = np.vstack([apsce_arr, np.array(data)])
    
    # Create a DataFrame to store the data #
    df = pd.DataFrame(apsce_arr, columns=['Company', 'Country', 'Sector', 'Exclusion Year'])
         
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'APSCE_EXCLUSIONS_\d+')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
    
    # Manipulate present and past columns #
    current_name = ''.join([word for word in list(df.columns) if 'COMPANY' in word.upper()])
    df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
    past_name = ''.join([word for word in list(df_past.columns) if 'COMPANY' in word.upper()])
    
    # Identify the list of additions and deletions #
    comparison = pd.merge(df, df_past, how = 'outer', left_on = current_name, right_on = past_name)
    add_list = comparison[comparison[past_name].isnull()][[current_name]]
    drop_list = comparison[comparison[current_name].isnull()][[past_name]]
    
    # Output the differences in text file #  
    download_updates('APSCE_UPDATES.txt', download_path, add_list, drop_list, 
                     current_name, past_name, encoding = None)
    download_updates('APSCE_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                     add_list, drop_list, current_name, past_name, encoding = None)

    # Download data #
    df.to_excel(download_path  + '\\' + filename, 
                sheet_name = 'APSCE_EXCLUSIONS', 
                index = False)
    

def get_FBP_companies(target_url, download_path, date_today):
    ''' 
    (1) Connect to Finance for Biodiversity Pledge (FBP) website and scrape the list of companies
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data 
    
    Parameters:
        target_url    : Url for companies in FBP
        download_path : Local file path to deposit companies in FBP
        date_today    : Batch run date
    ''' 
    # Create filename for respective entity #
    filename = 'FBP_COMPANIES_%s.xlsx' % date_today
                
    # Import data and rename columns #
    df = pd.read_html(target_url)[0]
         
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'FBP_COMPANIES_\d+')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
    
    # Manipulate present and past columns #
    current_name = ''.join([word for word in list(df.columns) if 'ORGANIZATION' in word.upper()])
    df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
    past_name = ''.join([word for word in list(df_past.columns) if 'ORGANIZATION' in word.upper()])
    
    # Identify the list of additions and deletions #
    comparison = pd.merge(df, df_past, how = 'outer', left_on = current_name, right_on = past_name)
    add_list = comparison[comparison[past_name].isnull()][[current_name]]
    drop_list = comparison[comparison[current_name].isnull()][[past_name]]
    
    # Output the differences in text file #  
    download_updates('FBP_UPDATES.txt', download_path, add_list, drop_list, 
                     current_name, past_name, encoding = None)
    download_updates('FBP_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                     add_list, drop_list, current_name, past_name, encoding = None)

    # Download data #
    df.to_excel(download_path  + '\\' + filename, 
                sheet_name = 'FBP', 
                index = False)


def get_ISIF_CM_Exclusions(target_url, download_path, date_today):
    ''' 
    (1) Connect to ISIF website and scrape the list of exclusion companies in Cluster Munitions
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data 
    
    Parameters:
        target_url    : Url for exclusion companies in ISIF's Cluster Munitions
        download_path : Local file path to deposit exclusion companies in ISIF's Cluster Munitions
        date_today    : Batch run date
    ''' 
    # Create filename for respective entity #
    filename = 'ISIF_CM_EXCLUSIONS_%s.xlsx' % date_today
                
    # Import data and rename columns #
    df = pd.read_html(target_url)[0]
    company_column = [column for column in list(df.columns) if 'COMPANY' in column.upper()] 
    df = df[company_column]
    df.columns = ['COMPANY']
    current_name = ''.join([word for word in list(df.columns)])
         
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'ISIF_CM_EXCLUSIONS_\d+')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
    
    # Manipulate present and past columns #
    company_column = [column for column in list(df_past.columns) if 'COMPANY' in column.upper()]
    df_past = df_past[company_column]
    df_past.columns = ['COMPANY_PAST']
    past_name = ''.join([word for word in list(df_past.columns)])

    # Identify the list of additions and deletions #
    comparison = pd.merge(df, df_past, how = 'outer', left_on = current_name, right_on = past_name)
    add_list = comparison[comparison[past_name].isnull()][[current_name]]
    drop_list = comparison[comparison[current_name].isnull()][[past_name]]
    
    # Output the differences in text file #  
    download_updates('ISIF_CM_UPDATES.txt', download_path, add_list, drop_list, 
                     current_name, past_name, encoding = None)
    download_updates('ISIF_CM_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                     add_list, drop_list, current_name, past_name, encoding = None)

    # Download data #
    df.to_excel(download_path  + '\\' + filename, 
                sheet_name = 'ISIF_CM_EXCLUSIONS', 
                index = False)


def get_ISIF_FF_Exclusions(filename, download_path, date_today):
    ''' 
    (1) Import the PDF list of ISIF Fossil Fuel exclusion companies from local directory
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data

    Parameters:
        filename      : Filename used to save the relevant raw data files
        download_path : Local file path that is used to deposit the relevant raw data files
        date_today    : Today's date
    '''   
    # Initialise parameters #
    df = pd.DataFrame()
    
    # Get last page number of PDF file #
    last_page = PyPDF2.PdfFileReader(open(download_path + '\\' + filename, 'rb')).getNumPages()
    
    # Parse the PDF file in the local directory #
    for page_num in range(1, last_page + 1):
        try:
            df_partial = tabula.read_pdf(download_path + '\\' + filename, pages=page_num)[0]
        except Exception:
            df_partial = tabula.read_pdf(download_path + '\\' + filename, pages=page_num)
        finally:
            # Concatenate the partial results with the full DataFrame #
            df = pd.concat([df, df_partial])        
            # Output status #
            print('Conversion Success: Page %s of %s' % (str(page_num), str(last_page)))
            
    if len(df) > 0:
        
        # Scan the directory and import the previous working day's dataset #
        latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'ISIF_FF_EXCLUSIONS_\d+.xlsx')
        df_past = pd.read_excel(download_path + '\\' + latest_file)
        
        # Manipulate present and past columns #
        df = df[df.columns[2:]]
        df.dropna(inplace=True)
        df.columns = ['COMPANY', 'COUNTRY']
        current_name = ''.join([word for word in list(df.columns) if 'COMPANY' in word])
        df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
        past_name = ''.join([word for word in list(df_past.columns) if 'COMPANY' in word])
        
        # Identify the list of additions and deletions #
        comparison = pd.merge(df, df_past, how = 'outer', left_on = current_name, right_on = past_name)
        add_list = comparison[comparison[past_name].isnull()][[current_name]]
        drop_list = comparison[comparison[current_name].isnull()][[past_name]]
        
        # Output the differences in text file #  
        download_updates('ISIF_FF_UPDATES.txt', download_path, add_list, drop_list, 
                         current_name, past_name, encoding = None)
        download_updates('ISIF_FF_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                         add_list, drop_list, current_name, past_name, encoding = None)
        
        # Download data #
        df.to_excel(download_path  + '\\' + re.sub('.pdf','.xlsx', filename), 
                    sheet_name = 'ISIF_FF_EXCLUSIONS', 
                    index = False)
        
    
def get_NBIM_Exclusions(target_url, download_path, date_today):
    ''' 
    (1) Connect to NBIM website and scrape the list of exclusion companies
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data 

    Parameters:
        target_url    : Url for NBIM's exclusion companies
        path          : Local file path to deposit NBIM's exclusion companies.
        date_today    : Batch run date
    ''' 
    # Initialize variables #
    NBIM_exclusions = {'COMPANY': [], 'FORMER_NAME': [], 'PRESS_RELEASE': [], 'CATEGORY': [], 
                       'CRITERION': [], 'DECISION': [], 'PUBLISHING_DATE': []}
    NBIM_columns_past = [column_name + '_PAST' for column_name in list(NBIM_exclusions.keys())]
    
    # Send request to NBIM website #
    response = requests.get(target_url, headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser')

    # Scoop the first table and associated rows #
    body = soup.find_all('tbody')
    table_rows = body[0].find_all('tr')
    for table_row in table_rows:
        # Scrape the list of exclusion companies and their associated details #
        company_name = table_row.find_all('a')
        row_data = table_row.find_all('span', attrs={'class':'nbim-responsive-table--value'})        
        if len(row_data) == 5:
            NBIM_exclusions['COMPANY'].append(company_name[0].text.strip())
            NBIM_exclusions['FORMER_NAME'].append('')
            NBIM_exclusions['PRESS_RELEASE'].append(company_name[0]['href'])
            NBIM_exclusions['CATEGORY'].append(row_data[1].text.strip())
            NBIM_exclusions['CRITERION'].append(row_data[2].text.strip())
            NBIM_exclusions['DECISION'].append(row_data[3].text.strip())
            NBIM_exclusions['PUBLISHING_DATE'].append(row_data[4].text.strip())
        elif len(row_data) == 6:
            NBIM_exclusions['COMPANY'].append(company_name[0].text.strip())
            NBIM_exclusions['FORMER_NAME'].append(row_data[1].text.strip())
            NBIM_exclusions['PRESS_RELEASE'].append(company_name[0]['href'])
            NBIM_exclusions['CATEGORY'].append(row_data[2].text.strip())
            NBIM_exclusions['CRITERION'].append(row_data[3].text.strip())
            NBIM_exclusions['DECISION'].append(row_data[4].text.strip())
            NBIM_exclusions['PUBLISHING_DATE'].append(row_data[5].text.strip())
        else:
            print(row_data, ' has an unacceptable number of data items. Please check!')

    # Import the dataset into DataFrame and pre-process the columns #
    df = pd.DataFrame(NBIM_exclusions)
    df['FORMER_NAME'] = df['FORMER_NAME'].str.replace('Former ', '')
    df['PRESS_RELEASE'] = df['PRESS_RELEASE'].str.replace('/link', 'https://www.nbim.no/link')
    
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'NBIM_EXCLUSIONS_\d+')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
    
     # Manipulate present and past columns #
    current_company = [column for column in list(df.columns) if 'COMPANY' in column.upper()][0]
    current_decision = 'DECISION'
    df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
    past_company = [column for column in list(df_past.columns) if 'COMPANY' in column.upper()][0]
    past_decision = current_decision + '_PAST'    
    
    # Identify the list of additions and deletions #
    comparison = pd.merge(df, df_past, how = 'outer', left_on = current_company, right_on = past_company)
    add_list = comparison[comparison[past_company].isnull()][list(NBIM_exclusions.keys())]
    drop_list = comparison[comparison[current_company].isnull()][NBIM_columns_past]
    
    # Identify the list of decision updates #
    comparison = pd.merge(df, df_past, 
                          how = 'left', 
                          left_on = [current_company, current_decision], 
                          right_on = [past_company, past_decision])
    decision_update_list = comparison[(comparison[past_company].isnull()) & \
                                     (~comparison[current_company].isin(list(add_list[current_company])))][list(df.columns)]
    decision_update_list = pd.merge(decision_update_list, df_past[[past_company, past_decision]], 
                                    how = 'left', 
                                    left_on = current_company,
                                    right_on = past_company)
    
    # Output the differences in text file #
    paths = [download_path, re.sub('Original', 'Editable', download_path)]
    for path in paths:
        with open(path + '\\' + 'NBIM_UPDATES.txt', 'a') as f:
            if len(add_list) != 0:
                for company in list(add_list[current_company]):
                    f.write(company + ' is added on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
            else:
                f.write('No company is added on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
            if len(drop_list) != 0:
                for company in list(drop_list[past_company]):
                    f.write(company + ' is removed on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
            else:
                f.write('No company is removed on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
            
            if len(decision_update_list) != 0:
                for company in list(decision_update_list[current_company]):
                    f.write('The decision status of ' + company + \
                            ' has been updated from ' +  \
                                decision_update_list.loc[
                                    (decision_update_list[current_company] == company)][past_decision].iloc[0] + \
                            ' to ' + \
                                decision_update_list.loc[
                                    (decision_update_list[current_company] == company)][current_decision].iloc[0] + \
                            ' on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
            else:
                f.write('No company has its decision status updated on ' + \
                        dt.datetime.now().strftime('%d.%m.%Y') + '\n')    
        f.close()
        
    # Download data #
    df.to_excel(download_path + '\\' + 'NBIM_EXCLUSIONS_%s.xlsx' % date_today, 
                sheet_name = 'NBIM_EXCLUSIONS', 
                index = False)


def get_NZMFAT_Exclusions(target_url, download_path, date_today):
    ''' 
    (1) Connect to New Zealand's Ministry of Foreign Affairs and Trade (NZMFAT) website 
        and scrape the list of exclusion companies/countries
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data

    Parameters:
        target_url    : Url for exclusion companies/countries in NZFAT
        download_path : Local file path to deposit exclusion companies/countries in NZFAT
        date_today    : Today's date
    '''   
    # Create filename for respective entity #
    filename = 'NZMFAT_EXCLUSIONS_%s.xlsx' % date_today
                
    # Send GET request #
    while True:
        try:
            response = requests.get(target_url, headers=headers)
            soup = BeautifulSoup(response.text, 'html.parser')
            table = soup.find_all('tbody')[0]
        except Exception:
            continue
        else:
            ## Break while loop ## 
            break
    
    # Scoop all the table rows for weblinks #
    weblinks = table.find_all('a', href=True)
    data = [[re.search(r'\d+', weblink['href']).group(0), 
             re.sub(r'\(external link\)', '', weblink.text).strip(), weblink['href']]
            for weblink in weblinks 
            if re.search(r'\d{4}', weblink['href'])]
    data = [[year, entity,'https://www.mfat.govt.nz' + weblink] 
            if not 'http://www.legislation.govt.nz/regulation' in weblink 
            else [year, entity, weblink] \
            for year, entity, weblink in data]
    
    # Initialise a placeholder #
    revised_data = []
    # For all weblinks that are not directed to NZ legislation, ... #
    # ... conduct another search on MFAT website for weblink #
    browser = start_webpage(log_path = download_path + '\\geckodriver.log')
    for year, org, weblink in data:
        if 'https://www.mfat.govt.nz' in weblink:    
            response = requests.get(weblink, headers=headers)
            soup = BeautifulSoup(response.text, 'html.parser')
            webs = soup.find_all('a', href=True)
            for web in webs:
                if 'http://www.legislation.govt.nz/regulation' in web['href']:
                    # Connect to NZ Legislation website to get name of UN Sanction # 
                    browser.get(web['href'])
                    time.sleep(1)
                    soup = BeautifulSoup(browser.page_source, 'html.parser')
                    revised_data.append([year, org, web['href'], soup.find('h1').text.strip()])
                    break
            # Remain as original weblink if legislation weblink cannot be found #
            if revised_data[-1][1] != org:
                revised_data.append([year, org, weblink, np.nan])
        else:
            # Connect to NZ Legislation website to get name of UN Sanction # 
            browser.get(weblink)
            time.sleep(1)
            soup = BeautifulSoup(browser.page_source, 'html.parser')
            revised_data.append([year, org, weblink, soup.find('h1').text.strip()])
    
    # Close browser #
    browser.close()
    
    # Import data and reorganise DataFrame #
    df = pd.DataFrame(revised_data, columns=['ENFORCEMENT_YEAR', 'ENTITY', 'LEGISLATION_WEBLINK', 'LEGISLATIVE_GROUND'])
    
    # Download latest country data from Britannica #
    countries = get_countries()
    
    # Merge both datasets to find matching countries and update row value as COUNTRY #
    merged = pd.merge(df, countries, how='left', left_on = 'ENTITY', right_on='COUNTRY')
    merged.loc[merged['COUNTRY'].notnull(), 'COUNTRY'] = 'COUNTRY'
    merged.columns = list(df.columns) + ['ENTITY_TYPE']
    
    # # Reorganise DataFrame to detect multiple organisations stored in single cell #
    # corpora = [(entity, nlp(entity)) for entity in list(merged.loc[merged['ENTITY_TYPE'].isnull(), 'ENTITY'])]
    # revised_ents = [[entity, ent.text.strip(), ent.label_] for entity, corpus in corpora for ent in corpus.ents]
    # organisations = pd.DataFrame(revised_ents)
    # organisations.columns = ['ENTITY', 'ENTITY_NEW', 'ENTITY_NEW_TYPE']
    # merged = pd.merge(merged, organisations, how='left', on='ENTITY')
    
    # Replace ENTITY column value with ENTITY_NEW column value #
    # merged.loc[merged['ENTITY_TYPE'].isnull() & merged.duplicated(subset=['ENTITY'], keep=False), 'ENTITY'] = \
    # merged.loc[merged['ENTITY_TYPE'].isnull() & merged.duplicated(subset=['ENTITY'], keep=False), 'ENTITY_NEW']        
    # # Replace ENTITY_TYPE column value with ENTITY_TYPE_NEW column value #
    # merged.loc[merged['ENTITY_TYPE'].isnull(), 'ENTITY_TYPE'] = \
    #    merged.loc[merged['ENTITY_TYPE'].isnull(), 'ENTITY_NEW_TYPE']
    # merged.loc[merged['ENTITY_TYPE'] == 'GPE', 'ENTITY_TYPE'] = 'OTHERS'
    # merged.loc[merged['ENTITY_TYPE'] == 'ORG', 'ENTITY_TYPE'] = 'ORGANISATION'
    # # Remove unnecessary columns #
    # del merged['ENTITY_NEW'], merged['ENTITY_NEW_TYPE']
    # Reorganise the remaining columns #    
    merged = merged[['ENFORCEMENT_YEAR', 'ENTITY', 'ENTITY_TYPE', 'LEGISLATION_WEBLINK', 'LEGISLATIVE_GROUND']] 
    
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'NZMFAT_EXCLUSIONS_\d+')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
    
    # Manipulate present and past columns #
    df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
    current_name = [column for column in list(merged.columns) 
                    if ('ENTITY' in column.upper()) and (not 'TYPE' in column.upper())][0]
    current_year = [column for column in list(merged.columns) if 'YEAR' in column.upper()][0]
    past_name = [column for column in list(df_past.columns) 
                 if ('ENTITY' in column.upper()) and (not 'TYPE' in column.upper())][0]
    past_year = [column for column in list(df_past.columns) if 'YEAR' in column.upper()][0]
    df_past[past_year] = df_past[past_year].astype(str)
    
    # Identify the list of additions and deletions #
    comparison = pd.merge(merged, df_past, 
                          how = 'outer', 
                          left_on = [current_name, current_year], right_on = [past_name, past_year])
    add_list = comparison[comparison[past_name].isnull()][[current_name]]
    drop_list = comparison[comparison[current_name].isnull()][[past_name]]
    
    # Output the differences in text file #  
    download_updates('NZMFAT_UPDATES.txt', download_path, add_list, drop_list, 
                     current_name, past_name, encoding = None)
    download_updates('NZMFAT_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                     add_list, drop_list, current_name, past_name, encoding = None)
    
    # Download data #
    merged.to_excel(download_path  + '\\' + filename, 
                    sheet_name = 'NZMFAT_EXCLUSIONS', 
                    index = False)
    

def get_NZSF_Exclusions(filename, download_path, date_today, add_path):
    ''' 
    (1) Connect to New Zealand's Super Fund's website and scrape the list of exclusion companies/countries
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data

    Parameters:
        filename      : Filename of raw data downloaded from NZSF
        download_path : Local file path to deposit exclusion companies/countries in NZSF
        date_today    : Today's date
        add_path      : Filepath (including filename) of file imported from NZ Ministry of Trade and Foreign Affairs
    ''' 
    # Import the datasets into Pandas DataFrame #               
    df_companies = pd.read_excel(download_path + '\\' + filename, sheet_name=0, usecols=[0], names=['ENTITY']) 
    df_countries = pd.read_excel(download_path + '\\' + filename, sheet_name=1, usecols=[0], names=['ENTITY'])
    df_NZMFAT = pd.read_excel(add_path, usecols=['ENTITY', 'LEGISLATIVE_GROUND'])
    
    # Initialise headers placeholder #
    headers = ['CLUSTER MUNITIONS', 'ANTI-PERSONNEL MINES', 'NUCLEAR BASE OPERATORS', 
               'MANUFACTURE OF CIVILIAN AUTOMATIC', 'POOR ESG PRACTICES',
               'RECREATIONAL CANNABIS', 'TOBACCO']
    
    # Pre-process companies dataset #
    # Find row numbers of each header (except Tobacco due to multiple entries) in headers #
    row_num_all = list(df_companies[df_companies['ENTITY'].str.contains('|'.join(headers[:-1]), case=False)].index)
    row_num_tobacco = list(df_companies.loc[df_companies['ENTITY'].str.upper() == headers[-1]].index)
    row_num_all += row_num_tobacco + [len(df_companies)]
    # Rearrange all entries in ascending order #
    row_num_all.sort()
    
    # Create a separate copy of df_companies #
    df = df_companies.copy()
    
    # Create a new column that contains reason for NZ Fund exclusion #
    df['EXCLUSION'] = df['ENTITY']
    for i in range(len(row_num_all) - 1):
        df.loc[row_num_all[i]:row_num_all[i+1] - 1, 'EXCLUSION'] = df.loc[row_num_all[i], 'ENTITY']
    # Remove all original headers in ENTITY column #
    df.drop(row_num_all[:-1], inplace=True)
    
    # Pre-process countries dataset #
    # Get row number for Notes #
    try:
        row_num = df_countries.loc[df_countries['ENTITY'].str.upper() == 'NOTES'].index[0]
    except Exception:
        df_countries = df_countries
    else:
        # Remove all records from Notes onwards #
        df_countries = df_countries.iloc[:row_num]
        
    # Pre-process NZMFAT dataset #
    df_countries['ENTITY'] = df_countries['ENTITY'].str.strip()
    temp = "The Democratic People's Republic of Korea"
    df_NZMFAT.loc[df_NZMFAT['ENTITY'].str.upper() == 'NORTH KOREA', 'ENTITY'] = temp 

    # Keep the first country record and drop subsequent duplicate countries #
    df_NZMFAT.drop_duplicates(subset=['ENTITY'], keep='first', inplace=True)
    
    # Merge df_countries and df_NZMFAT to create a column that contains reason for NZ Fund exclusion #
    df_countries = pd.merge(df_countries, df_NZMFAT, how='left', on='ENTITY')
    # Rename columns #
    df_countries.columns = ['ENTITY', 'EXCLUSION']
    
    # Append df_countries to df and remove additional spaces in EXCLUSION column #
    df = df.append(df_countries, ignore_index=True)
    df['EXCLUSION'] = df['EXCLUSION'].str.strip()
    
    # Drop duplicates #
    df.drop_duplicates(inplace=True)
    
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'NZSF_EXCLUSIONS_\d+')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
    
    # Manipulate present and past columns #
    df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
    current_name = [column for column in list(df.columns) if ('ENTITY' in column.upper())][0]
    past_name = [column for column in list(df_past.columns) if ('ENTITY' in column.upper())][0]
    
    # Identify the list of additions and deletions #
    comparison = pd.merge(df, df_past, how = 'outer', left_on = current_name, right_on = past_name)
    add_list = comparison[comparison[past_name].isnull()][[current_name]]
    drop_list = comparison[comparison[current_name].isnull()][[past_name]]

    # Output the differences in text file #  
    download_updates('NZSF_UPDATES.txt', download_path, add_list, drop_list, 
                     current_name, past_name, encoding = None)
    download_updates('NZSF_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                     add_list, drop_list, current_name, past_name, encoding = None)
    
    # Download data #
    df.to_excel(download_path  + '\\' + 'NZSF_EXCLUSIONS_%s.xlsx' % date_today, 
                sheet_name = 'NZSF_EXCLUSIONS', 
                index = False)
    
    
def get_SBTI_companies(filename, download_path, date_today):
    ''' 
    (1) Import the dataset into Pandas DataFrame
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data 

    Parameters:
        filename      : Name of file used to store SBTI's target companies
        download_path : Local file path to store SBTI's target companies
        date_today    : Batch run date
    '''
    # Import the dataset into Pandas DataFrame #               
    df = pd.read_csv(download_path + '\\' + filename)    
    
    # Pre-processing #
    ## Standardise the Target Classification ##
    df['Target Classification'] = df['Target Classification'].str.replace('\n', ' ')
    df['Target Classification'] = df['Target Classification'].str.replace(r'\s+', ' ')
    df['Target Classification'] = df['Target Classification'].str.strip()
    ## Standardise the Target ##
    df['Target'] = df['Target'].str.replace('"', '')
    df['Target'] = df['Target'].str.replace('\n', ' ')
    df['Target'] = df['Target'].str.replace('_x000D_', '')
    df['Target'] = df['Target'].str.replace(r'\s+', ' ')
    df['Target'] = df['Target'].str.strip()
    ## Standardise the Target Qualifications ##
    df['Target Qualification'].replace(np.nan, '', inplace=True)
    df['Target Qualification'] = df['Target Qualification'].astype(str)
    df['Target Qualification'] = df['Target Qualification'].str.replace('°', '')
    df.loc[df['Target Qualification'].str.contains('1.5'), 'Target Qualification'] = '1.5C'
    df.loc[df['Target Qualification'].str.contains('WB2C'), 'Target Qualification'] = 'Well-below 2C'
    df.loc[df['Target Qualification'].str.contains('below 2C'), 'Target Qualification'] = 'Well-below 2C'
    df['Target Qualification'].replace('', np.nan, inplace=True)
    df['Target Qualification'] = df['Target Qualification'].str.strip()
    ## Standardise the Date Committed / Target published ##
    df['Date'] = pd.to_datetime(df['Date'])
    dates = list(df['Date'])
    dates = [date.date() for date in dates]
    df['Date'] = dates
    
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'SBTI_COMPANIES_\d+\.xlsx')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
    df_past['Date'] = pd.to_datetime(df_past['Date'])
    dates = list(df_past['Date'])
    dates = [date.date() for date in dates]
    df_past['Date'] = dates  
    
    # Manipulate present and past columns #
    df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
    current_company = [column for column in list(df.columns) if 'COMPANY' in column.upper()][0]
    current_isin = 'ISIN'
    past_company = [column for column in list(df_past.columns) if 'COMPANY' in column.upper()][0]
    past_isin = current_isin + '_PAST'
    
    # Identify the list of entities with no changes to company name and ISIN #
    no_changes = pd.merge(df, df_past, how = 'inner', 
                          left_on = [current_company, current_isin], 
                          right_on = [past_company, past_isin])
    
    # Identify the list of ISIN updates #
    comparison = pd.merge(df, df_past, how = 'inner', left_on = current_company, right_on = past_company)
    isin_update_list = comparison.append(no_changes, ignore_index=True)
    isin_update_list.drop_duplicates(subset=[current_company, current_isin], keep=False, inplace=True)
    
    # Identify the list of company name updates #
    comparison = pd.merge(df, df_past, how = 'inner', left_on = current_isin, right_on = past_isin)
    name_update_list = comparison.append(no_changes, ignore_index=True)
    name_update_list.drop_duplicates(subset=[current_company, current_isin], keep=False, inplace=True)
    
    # Identify the list of additions and deletions #
    comparison = pd.merge(df, df_past, how = 'outer', left_on = current_company, right_on = past_company)
    add_list = comparison[(comparison[past_company].isnull()) & \
                          (~comparison[current_company].isin(list(name_update_list[current_company])))][list(df.columns)]
    drop_list = comparison[(comparison[current_company].isnull()) & \
                           (~comparison[past_company].isin(list(name_update_list[past_company])))][list(df_past.columns)]

    # Identify inconsistencies in Target Qualification #
    df_past.columns = [re.sub('_PAST', '', column_name) for column_name in df_past.columns]
    columns = [current_company, current_isin, 'Target Qualification', 'Date', 'Target']
    inconsistencies = (df.loc[df['Status'] == 'Targets Set', columns]
                       .append(df_past.loc[df_past['Status'] == 'Targets Set', columns],
                               ignore_index=True))
    inconsistencies.sort_values(by=current_company, inplace=True)
    inconsistencies.drop_duplicates(subset=columns, keep=False, inplace=True)
    exclusions = list(set(list(add_list[current_company]) + list(drop_list[past_company]) + \
                    list(df_past.loc[df_past['Status'] == 'Committed', 'Company Name'])))
    inconsistencies = inconsistencies.loc[~inconsistencies[current_company].isin(exclusions)]

    # Output the differences in text file #  
    paths = [download_path, re.sub('Original', 'Editable', download_path)]
    for path in paths:
        with open(path + '\\' + 'SBTI_UPDATES.txt', 'a', encoding = 'utf-8') as f:
            if len(isin_update_list) != 0:
                for company in list(isin_update_list[current_company]):
                    f.write('ISIN for ' + company + ' has been updated from ' + 
                            str(isin_update_list.loc[isin_update_list[current_company] == company, past_isin].iloc[0]) + \
                            ' to ' + \
                            str(isin_update_list.loc[isin_update_list[current_company] == company, current_isin].iloc[0]) + \
                            '\n')
            else:
                f.write('No company has its ISIN updated on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
        
            if len(name_update_list) != 0:
                for company in list(name_update_list[current_company]):
                    f.write('Company name has been updated from ' + 
                            str(name_update_list.loc[name_update_list[current_company] == company, past_company].iloc[0]) + \
                            ' to ' + company + '\n')
            else:
                f.write('No company has its name updated on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
        
            if len(add_list) != 0:
                for company in list(add_list[current_company]):
                    f.write(company + ' is added on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
            else:
                f.write('No company is added on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
            
            if len(drop_list) != 0:
                for company in list(drop_list[past_company]):
                    f.write(company + ' is removed on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
            else:
                f.write('No company is removed on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
            
            if len(inconsistencies) != 0:
                f.write('Inconsistencies detected on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
                inconsistencies.to_excel(download_path + '\\' + 'TARGET_QUAL_ERRORS_%s.xlsx' % date_today, 
                                         sheet_name = 'SBTI', 
                                         index = False)
            else:
                f.write('No inconsistencies are detected on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')

        f.close()
    
    # Download pre-processed file #
    df.to_excel(download_path + '\\' + 'SBTI_COMPANIES_%s.xlsx' % date_today, 
                sheet_name = 'SBTI', 
                index = False)


def get_SVVK_Exclusions(target_url, download_path, date_today):
    ''' 
    (1) Connect to SVVK ASIR website and scrape the list of exclusion companies
    (2) Identify the list of additions/deletions from last update
    (3) Download the relevant data 

    Parameters:
        target_url    : Url for exclusion companies in SVVK ASIR
        download_path : Local file path to deposit SVVK ASIR's exclusion companies
        date_today    : Batch run date
    ''' 
    # Create filename for respective entity #
    filename = 'SVVK_EXCLUSIONS_%s.xlsx' % date_today
    
    # Import the list of exclusion companies from SVVK website #
    df = pd.read_html(target_url, header=0)[0]
    df['EXCLUSION'] = np.nan
    
    # Preprocess DataFrame to retain marked companies for each exclusion criteria # 
    criteria = [column for column in list(df.columns) 
                if not re.search(r'NAME|DOMICIL|EXCLUSION', column.upper())]
    for seq, criterion in enumerate(criteria):
        df_temp = df.loc[df[criterion].notnull()]
        marked = list(df_temp.loc[df_temp[criterion].str.contains(r'\bmarked\b')].index)
        unmarked = list(df_temp.loc[~df_temp[criterion].str.contains(r'\bmarked\b')].index)
        df.iloc[marked, seq+2] = 'Yes'
        df.iloc[unmarked, seq+2] = np.nan
        ## Fill EXCLUSION column ##
        df.loc[(df[criterion].notnull()) & (df['EXCLUSION'].notnull()), 'EXCLUSION'] = \
            df.loc[(df[criterion].notnull()) & (df['EXCLUSION'].notnull()), 'EXCLUSION'] + ', ' + criterion
        df.loc[(df[criterion].notnull()) & (df['EXCLUSION'].isnull()), 'EXCLUSION'] = criterion
        
    # Scan the directory and import the previous working day's dataset #
    latest_file = get_latest_preceding_file(download_path,  date_today, regex_string = 'SVVK_EXCLUSIONS_\d+')
    df_past = pd.read_excel(download_path + '\\' + latest_file)
    
    # Manipulate past columns #
    df_past.columns = [column_name + '_PAST' for column_name in df_past.columns]
    current_name = [column for column in list(df.columns) if ('NAME' in column.upper())][0]
    past_name = [column for column in list(df_past.columns) if ('NAME' in column.upper())][0]
    
    # Identify the list of additions and deletions #
    comparison = pd.merge(df, df_past, how = 'outer', left_on = current_name, right_on = past_name)
    add_list = comparison[comparison[past_name].isnull()][[current_name]]
    drop_list = comparison[comparison[current_name].isnull()][[past_name]]
    
    # Output the differences in text file #  
    download_updates('SVVK_UPDATES.txt', download_path, add_list, drop_list, 
                     current_name, past_name, encoding = None)
    download_updates('SVVK_UPDATES.txt', re.sub('Original', 'Editable', download_path), 
                     add_list, drop_list, current_name, past_name, encoding = None)
    
    # Identify the updates in exclusion criteria #
    comparison = pd.merge(df, df_past, how = 'outer', left_on = current_name, right_on = past_name)
    criteria_delta = comparison.loc[(comparison['EXCLUSION'].notnull()) & \
                                    (comparison['EXCLUSION_PAST'].notnull()) & \
                                    (comparison['EXCLUSION'] != comparison['EXCLUSION_PAST'])]

    # Output the differences in text file #  
    paths = [download_path, re.sub('Original', 'Editable', download_path)]
    for path in paths:
        with open(path + '\\' + 'SVVK_UPDATES.txt', 'a') as f:
            if len(criteria_delta) > 0:
                for company in list(criteria_delta[current_name]):
                    f.write('Exclusion criteria for ' + company + ' has been updated from ' + \
                    str(criteria_delta.loc[criteria_delta[current_name] == company, 'EXCLUSION_PAST'].iloc[0]) + \
                    ' to ' + \
                    str(criteria_delta.loc[criteria_delta[current_name] == company, 'EXCLUSION'].iloc[0]) + '\n')
            else:
                f.write('No company has its exclusion criteria updated on ' + dt.datetime.now().strftime('%d.%m.%Y') + '\n')
    
    # Download data #
    df.to_excel(download_path  + '\\' + filename, 
                sheet_name = 'SVVK_EXCLUSIONS', 
                index = False)
      

# Get the outstanding list of countries for matching purposes #
def get_countries(source='https://www.britannica.com/topic/list-of-countries-1993160'):
    ''' 
    Get all the up-to-date countries from Britannica website
    Parameter:
        source        : Url containing the list of countries.
                        Default value is https://www.britannica.com/topic/list-of-countries-1993160
    Return:
        countries     : DataFrame containing the up-to-date countries found in Britannica      
    ''' 
    # Send GET response #
    response = requests.get(source, headers=headers)
    soup = BeautifulSoup(response.text, 'html.parser')
    
    # Scoop all the urls and extract the country data #
    urls = soup.find_all('a', attrs={'class': 'md-crosslink'})
    countries = [url.text.strip() for url in urls if not re.search(r'government|geography', url.text)]
    
    # Pre-process countries to change order of country names for countries with a comma punctuation ... #
    # and remove additional spaces #
    countries = [country.split(',')[-1].strip() + ' ' + country.split(',')[0].strip() 
                 if re.search(',', country) 
                 else country
                 for country in countries]
    countries = [re.sub(r'\s{2,}', ' ', country) for country in countries]
    countries = countries + ["The Democratic People's Republic of Korea "]
    
    # Import the data into DataFrame #
    countries = pd.DataFrame([countries]).T
    countries.columns = ['COUNTRY']
    
    return countries


# Scan all the UPDATES.txt files and determine course of action required #
def scan_for_rerun(source, rerun_path):
    ''' 
    Scan all the UPDATES.txt files and manual Excel file for follow-up cases
    
    Parameters:
        source        : Source table that contains organisations and their associated weblink
        rerun_path    : Excel file that contains follow-up cases manually inserted by user
    '''   
    # Initialise parameter: scan_counter #
    scan_counter = True
    while scan_counter:    
        # Initialise today's date in specified format DD.MM.YYYY #
        date_today = dt.datetime.now().strftime('%d.%m.%Y')
        # Initialise follow_up_cases #
        follow_up_cases = []
        
        # Consolidate all UPDATES.txt #
        update_files = [(source['ORGANISATION'].iloc[i], source['LOCAL_DOWNLOAD_PATH'].iloc[i] + '\\' + file) 
                        for i in range(len(source))
                        for file in os.listdir(source['LOCAL_DOWNLOAD_PATH'].iloc[i])
                        if re.search('UPDATES.txt', file)] 
        
        # Check each text file to verify whether today's date is captured #
        for org, update_file in update_files:        
            f = open(update_file, 'r', errors='ignore')
            no_follow_up = [org for line in f.readlines() if date_today in line]
            f.close()
            # Append follow_up_case in follow_up_cases #
            if len(no_follow_up) == 0:
                follow_up_cases.append(org)
            
        # Import manually inserted re-run cases from Excel: re_run #
        re_run = pd.read_excel(rerun_path, sheet_name='ORGANISATIONS')
        
        # Check manually inserted cases and schedule for re-run if required #
        if len(re_run) > 0:
            select_counter = True
            while select_counter:
                cases = ', '.join(list(re_run['ORGANISATION']))
                selection = input('There is(are) case(s) in the Excel file re-run.xlsx: %s \n' % cases + \
                                  'Select Y to re-run or N to terminate this part of the process. \n')
                if selection.upper() == 'Y':
                    # Merge the cases with the source table #   
                    source = pd.merge(re_run, source, how='left', on='ORGANISATION')
                    # Re-run download process #
                    download_file(source, attachments=True)
                    download_file(source, attachments=False)
                    # Toggle selection_counter to False to exit inner while loop #
                    select_counter = False
                    
                elif selection.upper() == 'N':
                    # Clean up all the records # 
                    print('Initiated the removal of cases from re_run.xlsx')
                    re_run.iloc[0:0].to_excel(rerun_path, 
                                              sheet_name='ORGANISATIONS',
                                              index=False)
                    print('All the cases have been removed from re_run.xlsx')
                    # Toggle selection_counter to False to exit inner while loop #
                    select_counter = False
                
                else:
                    print('You have entered an invalid response. Please try again!')
        
        # Check follow_up_cases and schedule for re-run #
        if len(follow_up_cases) > 0:
            select_counter = True
            while select_counter:
                cases = ', '.join(follow_up_cases)
                selection = input('There is(are) organisation(s) where there are no updates: %s \n' % cases + \
                                  'Select Y to re-run or N to terminate this part of the process. \n')
                if selection.upper() == 'Y':
                    # Merge the cases with the source table #
                    follow_up = pd.DataFrame([follow_up_cases], columns=['ORGANISATION'])    
                    source = pd.merge(follow_up, source, how='left', on='ORGANISATION')
                    # Re-run download process #
                    download_file(source, attachments=False)
                    download_file(source, attachments=True)
                    # Toggle selection_counter to False to exit inner while loop #
                    select_counter = False
                
                elif selection.upper() == 'N':
                    # Nullify the follow_up_cases #
                    follow_up_cases = []
                    # Toggle selection_counter to False to exit inner while loop #
                    select_counter = False
                    
                else:
                    print('You have entered an invalid response. Please try again!')
                    
        if len(follow_up_cases) == 0 and len(re_run) == 0:
            # Output scan results #
            print('Scan completed. No exceptional cases are noted!')
            # Toggle scan_counter to False to exit outer while loop #
            scan_counter = False


# Push out files from Original to Editable folder in same path #
def migrate_files(source):
    ''' 
    Identify and push out relevant files from Original to Editable folder
    Parameter:
        source        : Source table that contains organisations and their associated weblink
    '''   
    # Initialise parameters #
    raw_filepaths = list(source['LOCAL_DOWNLOAD_PATH'])
    
    for raw_filepath in raw_filepaths:
        # Create a list for raw files in Original folder #
        raw_files = [file for file in os.listdir(raw_filepath)]
        # Create a list for files in Editable folder #
        out_filepath = re.sub('Original', 'Editable', raw_filepath)
        out_files = [file for file in os.listdir(out_filepath)]
        
        # Output the list of new file(s) found in Original folder but not in Editable folder #
        new_files = [file for file in raw_files if file not in out_files]        
        for new_file in new_files:
            shutil.copy2(raw_filepath + '\\' + new_file, out_filepath)
            # Print completion message #
            print('Completed migration of %s from %s to %s' %(new_file, raw_filepath, out_filepath))


# Send email to recipients for manual follow-up #
def send_mail(source, username, password, recipient, subject='Company Updates'):
    ''' 
    Scan all the UPDATES.txt files and highlight updates in email
    Parameters:
        source        : Source table that contains organisations and their associated weblink
        username      : Outlook 365 userid
        password      : Outlook 365 password
        recipient     : List of intended recipients
        subject       : Subject of email.
                        Default value is Updates on Exclusion Companies
    '''   
    # Initialise parameters #
    follow_up_cases = []
    date_today = str(dt.datetime.now().strftime('%d.%m.%Y'))
    message = '**NEW UPDATES**' + '\n' + '\n'
    count = 1
    
    # Sieve out latest updates for all UPDATES.txt #
    email_orgs = list(source.loc[source['SEND_EMAIL'].notnull(), 'ORGANISATION'])
    emails = [(org, source.loc[source['ORGANISATION'] == org, 'LOCAL_DOWNLOAD_PATH'].iloc[0]) 
               for org in email_orgs]
    for org, update_path in emails:
        for file in os.listdir(update_path):
            # Identify UPDATES file #
            if re.search(r'UPDATES.txt', file):
                # Open file #
                f = open(update_path + '\\' + file, 'r', errors='ignore')
                # Parse the content inside the file #
                follow_up_cases.append([(org, line) for line in f.readlines() 
                                        if not re.search(r'No co|incon', line) and date_today in line])
                # Close file #
                f.close()
                # Exit inner for loop #
                break
            
    # Check the number of follow_up_cases and send email to intended recipients #
    for cases in follow_up_cases:
        if len(cases) > 0:
            # Initialise parameters #
            msg = MIMEMultipart()
            msg['From'] = username
            msg['To'] = recipient
            msg['Subject'] = subject + ' - ' + date_today
            
            # Update message #
            for org, line in cases:
                message += str(count) + '. ' + org + ' : ' + line + '\n'
                count += 1
                
            message += ('\n' + 'You may wish to look in the respective local folders ' + \
                        'or websites for new press releases.' + \
                        '\n' + '\n' + \
                        'THIS IS AN AUTOMATED MESSAGE. PLEASE DO NOT REPLY DIRECTLY TO THIS EMAIL!')
            msg.attach(MIMEText(message))
    
            try:
                print('Sending email to ' + recipient)

                mailServer = smtplib.SMTP('smtp-mail.outlook.com', 587)
                mailServer.ehlo()
                mailServer.starttls()
                mailServer.ehlo()
                mailServer.login(username, password)
                mailServer.sendmail(username, recipient, msg.as_string())
                mailServer.close()
        
                print('Email sent to ' + recipient)
        
            except Exception as e:
                print(e) 
       

def main():
    ''' 
    Main function
    '''          
    # Ingest data from latest source file #
    source_destination = r'C:\Risk\Dropbox (SciBetaTeam)\SciBetaTeam\Website Contents' + '\\'
    source_file = 'web_scrapping_urls.xlsx'
    
    config = pd.read_excel(source_destination + source_file,
                           sheet_name='CONFIG',
                           index_col=0)
    source = pd.read_excel(source_destination + source_file, 
                           sheet_name='ORGANISATIONS')
    source['SCRAPE_LINK'] = source['ORG_LINK']
    
    # Exclude organisations without downloadable attachments, i.e. no ATTACH_FLAG #
    pdf_orgs = source.dropna(subset=['ATTACH_FLAG'])
    target_urls = [(pdf_orgs['ORGANISATION'].iloc[i], pdf_orgs['ORG_LINK'].iloc[i]) 
                    for i in range(len(pdf_orgs))]
    
    # For organisations with downloadable attachments, replace urls with latest weblinks (if any) #
    for org, url in target_urls:
        # Run enrich_url function #
        data = enrich_urls(org, url)
        if len(data) != 0:
            for organisation, data_url in data:
                # Update WEBLINK column of source table #
                source.loc[source['ORGANISATION'] == organisation, 'SCRAPE_LINK'] = data_url
    
    # Download refreshed source table into Dropbox #
    try:    
        with pd.ExcelWriter(source_destination + source_file) as writer:
            source.to_excel(writer, 
                            sheet_name='ORGANISATIONS', 
                            index=False)
            config.to_excel(writer, 
                            sheet_name='CONFIG')
    except Exception as e:
        # Print error message #
        print(e)   
   
    # Download the exclusion attachments into Dropbox #
    download_file(source, attachments=False)
    download_file(source, attachments=True)
    
    # Scan for all UPDATES.txt and re_run.xlsx file and trigger re-run if required # 
    scan_for_rerun(source, source_destination + config.loc['file_rerun'].iloc[0])    
    
    # Push out files from Original to Editable folder in same path #
    migrate_files(source)
    
    # Send automated email if there are updates #
    username = config.loc['userid'].iloc[0]
    password = config.loc['password'].iloc[0]
    recipients = config.loc['email_recipients'].iloc[0].split(', ')
    for recipient in recipients:    
        send_mail(source, username, password, recipient = recipient)
    
if __name__ == '__main__':
      main()